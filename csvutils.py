
import csv
import sys


def OpenFileOrStdin(path):
    if path == '-':
        return sys.stdin
    else:
        return open(path, 'r')


def OpenFileOrStdout(path):
    if path == '-':
        return sys.stdout
    else:
        return open(path, 'w')


def LoadCsv(path):
    with OpenFileOrStdin(path) as fp:
        r = csv.reader(filter(lambda line: line.strip() and not line.startswith('#'), fp.readlines()), skipinitialspace=True)
        return r


def WriteCsv(path, rows):
    with OpenFileOrStdout(path) as fp:
        w = csv.writer(fp)
        w.writerows(rows)
