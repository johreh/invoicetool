#!/usr/bin/env python

import argparse
import datetime
import json
import locale
import sys

from .csvutils import LoadCsv, WriteCsv


def LoadPrices(pricefile):
    pricedb = {}
    pricejson = json.load(pricefile)
    for block in pricejson:
        startDate = datetime.date.fromisoformat(block['startDate'])
        prices = pricedb.setdefault(startDate, {})
        for priceentry in block['prices']:
            key = priceentry['key']
            price = float(priceentry['value']) / 1.25
            prices[key] = price

    cumulativeprices = {}
    for date in sorted(pricedb.keys()):
        cumulativeprices.update(pricedb[date])
        pricedb[date] = dict(cumulativeprices)

    return pricedb


def LookupPricees(date, pricedb):
    startdates = sorted(list(pricedb.keys()), reverse=True)
    for startdate in startdates:
        if date >= startdate:
            return pricedb[startdate]

    raise KeyError(date)


def FormatInvoiceRows(defaultdate, rows, parameters, pricedb):
    date_column = 1
    for row in rows:
        try:
            row[date_column].format_map(parameters)

            if not row[date_column].strip():
                row[date_column] = defaultdate.isoformat()

            date = datetime.date.fromisoformat(row[date_column])

            row_parameters = dict(parameters)
            row_parameters.update(LookupPricees(date, pricedb))
            row_parameters.update({
                'row_day': str(date.day),
                'row_month': date.strftime('%B'),
                'row_year': str(date.year)
                })

            for column in range(len(row)):
                row[column] = row[column].format_map(row_parameters)

            yield row

        except:
            print(f"Error in row: {row}")
            raise


def main():
    locale.setlocale(locale.LC_ALL, 'sv_SE.utf8')
    today = datetime.date.today()
    first_of_this_month = today.replace(day=1)

    parser = argparse.ArgumentParser(description='Format a CSV template', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--date', default=str(first_of_this_month), help='YYYY-mm-dd date to use for default date')
    parser.add_argument('--parameter-file', default='parameters.json', help='JSON with text substitution parameter values')
    parser.add_argument('--price-file', default='prices.json', help='JSON with price data')
    parser.add_argument('invoicedata', help='Template CSV file to format, - for stdin')
    parser.add_argument('out', help='Formatted output CSV file, - for stdout')

    options = parser.parse_args()

    with open(options.parameter_file) as paramf, open(options.price_file) as pricef:
        invoicerows = LoadCsv(options.invoicedata)
        parameters = json.load(paramf)
        pricedb = LoadPrices(pricef)
        formattedrows = FormatInvoiceRows(options.date, invoicerows, parameters, pricedb)

        WriteCsv(options.out, formattedrows)


if __name__ == '__main__':
    main()
