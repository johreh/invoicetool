#!/usr/bin/env python

import argparse
import csv
import datetime
import itertools
import json
import locale
import logging
import operator
import re
import sys

from dateutil.relativedelta import relativedelta

from .csvutils import LoadCsv, WriteCsv
from .generate import GenerateRows
from .invoicetemplate import LoadPrices, FormatInvoiceRows


config = {
    'userdb':                   'members.json',
    'efaktura_json':            'efaktura.json',
    'parameters':               'parameters.json',
    'prices':                   'prices.json',
    'rules':                    'rules.json',
    'id_file':                  'nextinvoice.txt',
}

log = logging.getLogger('Invoice tool')
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler())


class FormatDefault(dict):
    """
    Dict that returns "{key}" for any missing keys. Used for string formatting
    where format parameters are not all available yet.
    """
    def __missing__(self, key):
        return "{" + key + "}"


def LoadUserDb(path):
    db = json.load(path)
    userdict = {entry['crNumber'].strip(): entry for entry in db}
    return userdict


def To12digitCivicnr(civicnr):
    currentyear = datetime.date.today().year % 1000
    currentcentury = int(datetime.date.today().year / 100)
    lastcentury = currentcentury - 1

    nrlen = len(re.sub(r'\D', '', civicnr))
    if nrlen == 10:
        birthyear = int(civicnr[:2])
        # Let's coldly calculate with no members older than 100
        return str(currentcentury if (birthyear < currentyear) else lastcentury) + civicnr
    elif nrlen == 12:
        return civicnr
    else:
        raise Exception(f'Incorrectly formatted civic nr: {civicnr}')


def invoiceToCompany(userdb, civicnr, category):
    """
    Check if a catogory should be invoiced to the "company" entry for the user
    """

    if not userdb[civicnr].get('invoiceToCompany', False):
        return False

    includedCategories = [c.strip().lower() for c in userdb[civicnr].get('includeCategoriesToCompany', [])]
    excludedCategories = [c.strip().lower() for c in userdb[civicnr].get('excludeCategoriesToCompany', [])]

    if category.strip().lower() in excludedCategories:
        return False
    else:
        return (not includedCategories) or (category.strip().lower() in includedCategories)


def PrepareEfaktura(users, nextid, invoice_data, due_date):
    invoices = {}
    for row in invoice_data:
        try:
            [civicnr, date, desc, quantity, unitprice, category, tax_table, account] = row
        except ValueError:
            log.error(f'Incorrect invoice row: {row}')
            raise

        civicnr = civicnr.strip()
        if civicnr not in users:
            log.error('Invoice data contains rows for user {civicnr.strip()} not in database')

        if invoiceToCompany(users, civicnr, category.strip()):
            recipient = users[civicnr].get('company', {}).get('orgNum', civicnr)
        else:
            recipient = To12digitCivicnr(civicnr)

        invoicerows = invoices.setdefault(recipient, [])

        invoicerows.append({'date':         date.strip(),
                            'description':  desc.strip(),
                            'quantity':     locale.str(float(quantity.strip())),
                            'unitprice':    locale.str(float(unitprice.strip())),
                            'tax':          tax_table,
                            'account':      account })

    invoices = [ { 'civicnr': recipient,
                   'refnum': '{:0>6}'.format(invoiceid),
                   'duedate': due_date.isoformat(),
                   'rows': sorted(rows, key=operator.itemgetter('date', 'account', 'tax', 'description')) }
                 for (invoiceid, (recipient, rows))
                 in zip(itertools.count(nextid), invoices.items()) ]

    return sorted(invoices, key=operator.itemgetter('civicnr'))


def concat(xss):
    return sum(xss, [])


def PrepareInvoices(defaultdate, duedate, startid, userdb, invoice_files, export_efaktura, export_invoiceid, parameters, prices):
    invoice_template = concat([list(LoadCsv(f)) for f in invoice_files])
    invoice_data = list(FormatInvoiceRows(defaultdate, invoice_template, parameters, prices))

    efaktura_data = PrepareEfaktura(userdb, startid, invoice_data, duedate);
    with open(export_efaktura, 'w') as f:
        json.dump(efaktura_data, f, indent=4);

    next_invoiceid = max([int(invoice['refnum']) for invoice in efaktura_data]) + 1
    with open(export_invoiceid, 'w') as f:
        f.write(str(next_invoiceid))


def CreateArgParser():
    today = datetime.date.today()
    first_of_this_month = today.replace(day=1)
    last_of_last_month = first_of_this_month - relativedelta(days=1)
    first_of_last_month = last_of_last_month.replace(day=1)
    first_of_next_month = first_of_this_month + relativedelta(months=1)
    last_of_this_month = first_of_next_month - relativedelta(days=1)
    in_15_days = today + datetime.timedelta(days=15)
    default_due_date = max(last_of_this_month, in_15_days)

    parser = argparse.ArgumentParser(description='Batch create and send invoices', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    commands = parser.add_subparsers()

    generatecmd = commands.add_parser('generate', help='Generate invoice data', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    generatecmd.set_defaults(generate=True)
    generatecmd.add_argument('--userdb', default=config['userdb'], help='JSON user databse')
    generatecmd.add_argument('--date', default=str(first_of_this_month), help='YYYY-mm-dd date at which to invoice for, used for checking agreement validity')
    generatecmd.add_argument('--end-of-period', default=str(last_of_this_month), help='YYYY-mm-dd end of period to invoice for (inclusive)')
    generatecmd.add_argument('invoicedata', help='CSV file to write with fees')

    createcmd = commands.add_parser('create', help='Create invoices', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    createcmd.set_defaults(create=True)
    createcmd.add_argument('--id', required=True, type=int, help='ID of first invoice in series')
    createcmd.add_argument('--userdb', default=config['userdb'], help='JSON user databse')

    createcmd.add_argument('--efaktura-file', default=config['efaktura_json'], help='file to store e-faktura json in')
    createcmd.add_argument('--id-file', default=config['id_file'], help='file to store next invoid id in sequence to')
    createcmd.add_argument('--parameter-file', default=config['parameters'], help='JSON with text substitution parameter values, used for email file and invoice-data files')
    createcmd.add_argument('--price-file', default=config['prices'], help='JSON with price data')
    createcmd.add_argument('--date', default=str(first_of_this_month), help='YYYY-mm-dd date this invoice is for')
    createcmd.add_argument('--due-date', default=str(default_due_date), help='Invoice due-date in YYYY-mm-dd format')
    createcmd.add_argument('invoicedata', nargs='+', help='CSV file containing data to create invoices from')

    return parser

def main():
    parser = CreateArgParser()
    options = parser.parse_args()

    locale.setlocale(locale.LC_ALL, 'sv_SE.utf8')

    if 'generate' in options:
        with open(options.userdb) as udbf:
            users = LoadUserDb(udbf)
            date = datetime.date.fromisoformat(options.date)
            end_of_period = datetime.date.fromisoformat(options.end_of_period)

            sys.stderr.write('Generating invoice data for period ')
            sys.stderr.flush()
            sys.stdout.write(f'{date}:{end_of_period}\n')
            sys.stdout.flush()

            invoice_rows = GenerateRows(users, date, end_of_period)
            WriteCsv(options.invoicedata, invoice_rows)

    elif 'create' in options:
        with open(options.userdb) as udbf, open(options.parameter_file) as paramf, open(options.price_file) as pricef:
            invoicedate = datetime.date.fromisoformat(options.date)
            due_date = datetime.date.fromisoformat(options.due_date)

            parameters = json.load(paramf)
            prices = LoadPrices(pricef)
            users = LoadUserDb(udbf)
            sys.stderr.write(f'Creating invoices for {invoicedate} due {options.due_date}\n')
            PrepareInvoices(invoicedate, due_date, options.id, users, options.invoicedata, options.efaktura_file, options.id_file, parameters, prices)

    else:
        parser.print_usage(file=sys.stderr)

if __name__ == '__main__':
    main()

