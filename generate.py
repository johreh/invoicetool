"""
Generate fees from a member database using defined rules
"""

import rules

import datetime
import enum
from dateutil.relativedelta import relativedelta


class PeriodDate(enum.Enum):
    Start = enum.auto()
    End = enum.auto()


def IsAgreementActive(agreement, period_start, period_end):
    r"""
    Returns True if given agreement is active at some point in the provided
    period (both ends inclusive)
    """
    agreement_start = datetime.date.fromisoformat(agreement['startDate'])
    if 'endDate' in agreement:
        agreement_end = datetime.date.fromisoformat(agreement['endDate'])
        return agreement_start <= period_end and agreement_end >= period_start
    else:
        return agreement_start <= period_end


def IsAgreementPartial(agreement, period_start, period_end):
    r"""
    Returns a pair of booleans signifiying if agreement starts or ends
    within provided period.
    """
    agreement_start = datetime.date.fromisoformat(agreement['startDate'])
    starts_in_period = (agreement_start >= period_start) and (agreement_start <= period_end)
    if 'endDate' in agreement:
        agreement_end = datetime.date.fromisoformat(agreement['endDate'])
        ends_in_period = (agreement_end >= period_start) and (agreement_end <= period_end)
        return (starts_in_period, ends_in_period)
    else:
        return (starts_in_period, False)


def GetPeriodDates(member_entry, period_start, period_end):
    dates = set([(period_start, PeriodDate.Start.value), (period_end, PeriodDate.End.value)])

    agreements = member_entry['agreements']
    for entry in agreements:
        sip, eip = IsAgreementPartial(entry, period_start, period_end)
        if sip:
            dates.add((datetime.date.fromisoformat(entry['startDate']), PeriodDate.Start.value))
        if eip:
            dates.add((datetime.date.fromisoformat(entry['endDate']), PeriodDate.End.value))

    return sorted(dates)


def SplitPeriod(member_entry, period_start, period_end):
    dates = GetPeriodDates(member_entry, period_start, period_end)
    start_date, _ = dates.pop(0)
    while dates:
        peek_date, period_type = dates[0]

        if period_type == PeriodDate.Start.value:
            end_date = peek_date - relativedelta(days=1)
            yield (start_date, end_date)
            start_date, _ = dates.pop(0)

        else:  # period_type == PeriodDate.End.value
            end_date, _ = dates.pop(0)
            yield (start_date, end_date)
            start_date = end_date + relativedelta(days=1)


def SplitMonths(period_start, period_end):
    while period_end.month > period_start.month:
        next_month_start = period_start.replace(day=1) + relativedelta(months=1)
        month_end = next_month_start - relativedelta(days=1)

        yield (period_start, month_end)
        period_start = next_month_start

    yield (period_start, period_end)


def GenerateRows(userdb, period_start, period_end):
    for member in userdb.values():
        months = period_end - period_start

        for (period_start_, period_end_) in SplitMonths(period_start, period_end):
            start_of_month = period_start_.replace(day=1)
            days_month = ((start_of_month + relativedelta(months=1)) - start_of_month).days

            for (period_start__, period_end__) in SplitPeriod(member, period_start_, period_end_):
                num_days = ((period_end__ - period_start__) + relativedelta(days=1)).days
                active_agreements = [entry for entry in member['agreements'] if IsAgreementActive(entry, period_start__, period_end__)]
                for fee in rules.fees(active_agreements, num_days, days_month):
                    invoicerow = [ member['crNumber'],
                                   period_start__.isoformat(),
                                   fee['description'].strip(),
                                   round(fee['multiplier'], 2),
                                   fee['fee'],
                                   fee['category'],
                                   fee['tax_table'],
                                   fee['account'] ]
                    yield invoicerow

