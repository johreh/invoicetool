"""
Diff two invoice csv files and produce a new file that makes up the differene
from the first to the second.
"""

import argparse
from .csvutils import LoadCsv, WriteCsv


QUANTITY_COLUMN = 3


def SameItem(a, b):
    """
    Check if two invoice rows are the same except for the quantity column.
    """
    qa = a[QUANTITY_COLUMN]
    qb = b[QUANTITY_COLUMN]
    a[QUANTITY_COLUMN] = 0
    b[QUANTITY_COLUMN] = 0
    same = (a == b)
    a[QUANTITY_COLUMN] = qa
    b[QUANTITY_COLUMN] = qb
    return same


def DiffRows(predicted, actual):
    """
    Given two lists of invoice rows, return a list of invoice rows that if
    added to the predicted invoice will yield a sum that is equal to actual
    invoice.
    """
    for row in list(predicted):
        if row in list(actual):
            # Exact row on both invoices
            predicted.remove(row)
            actual.remove(row)

    compensated_rows = []
    for predicted_row in list(predicted):
        matching = [row for row in actual if SameItem(row, predicted_row)]
        if len(matching) == 1:
            # A uniquely identifiable row is on both predicted and actual invoice
            # with only quantity differing.
            #
            # Calculate actual fee - invoiced fee
            actual_row = matching[0]
            predicted.remove(predicted_row)
            actual.remove(actual_row)
            predicted_q = float(predicted_row[QUANTITY_COLUMN])
            actual_q = float(actual_row[QUANTITY_COLUMN])
            actual_row[QUANTITY_COLUMN] = round(actual_q - predicted_q, 2)
            yield actual_row

    for row in predicted:
        # Row only on predicted invoice, remove
        predicted_q = float(row[QUANTITY_COLUMN])
        row[QUANTITY_COLUMN] = round(-predicted_q, 2)
        yield row

    for row in actual:
        # Row only on actual invoice, add
        yield row


def LoadInvoice(filepath):
    """
    Load an invoice CSV and return rows grouped by recipient
    """
    csv = LoadCsv(filepath)
    invoices = {}
    for row in csv:
        invoices.setdefault(row[0], []).append(row)

    return invoices


def main():
    parser = argparse.ArgumentParser(description='Diff invoices', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('predicted', help='Invoice with predicited fees')
    parser.add_argument('actual', help='Create a diff to make predicted invoice match this one')
    parser.add_argument('out', help='Output invoice with adjustment rows')
    options = parser.parse_args()

    predicted = LoadInvoice(options.predicted)
    actual = LoadInvoice(options.actual)

    adjusted_rows = []
    for recipient in set(list(predicted.keys()) + list(actual.keys())):
        a = predicted.get(recipient, [])
        b = actual.get(recipient, [])
        adjusted_rows.extend(DiffRows(a, b))

    WriteCsv(options.out, adjusted_rows)


if __name__ == '__main__':
    main()
